$:.push File.expand_path("../lib", __FILE__)

require 'natpmp/version'

NATPMP::GemSpec = Gem::Specification.new do |s|
  s.name        = %q{natpmp}
  s.version     = NATPMP::Version
  s.required_ruby_version = ">=1.9.2"
  s.date        = Time.now.strftime("%Y-%m-%d")
  s.authors     = ['Nick Townsend']
  s.email       = ['nick.townsend@mac.com']
  s.summary     = %q{Client NAT-PMP protocol}
  s.homepage    = %q{https://github.com/townsen/natpmp}
  s.has_rdoc    = false
  s.license     = 'MIT'
  s.executables << 'natpmp'
  s.files       = Dir['lib/**/*.rb'] + Dir['test/**/*.rb']
  s.files       += %w{ Gemfile Rakefile LICENSE README.md .gemtest }
  s.description = "Client Interface to NAT Port Mapping Protocol"
  s.add_development_dependency 'minitest', '~> 4.3', '>= 4.3.2'
  s.add_development_dependency 'rake-compiler', '~> 0.9', '>= 0.9.2'
end
